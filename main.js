/**
 * Handle logo swapping and panel scroll.
 */

document.getElementById( 'menu-toggle' ).addEventListener( 'click' , function () {
    document.body.classList.toggle( 'menu-open' );
    this.classList.toggle( 'fa-bars' );
    this.classList.toggle( 'fa-close' );
});

function allNavigate (element) {
    function siteMenuNavigate ( event ) {
        window.location = event.target.attr( 'href' );
    }
    element.addEventListener( 'click' , siteMenuNavigate);
    element.addEventListener( 'touchend' , siteMenuNavigate);
}

document.querySelectorAll( '#site-menu a' ).forEach(allNavigate);
document.querySelectorAll( '#services-nav a' ).forEach(allNavigate);

var homepageSkip = document.getElementById( 'skip-to-main' );
if ( homepageSkip ) {
    homepageSkip.addEventListener( 'click' , function ( event ) {
        event.preventDefault();
        document.getElementById( 'main' ).scrollIntoView({ behavior: 'smooth', block: 'start' });
    });
}