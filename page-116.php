<?php
/*
 * Template Name: Technically Home
 */

get_header();
?>
        <article id="mission-statement">
            <h2><?php the_field('mission_statement'); ?></h2>
        </article>
        <article id="locations">
            <figure class="illustration">
                <img id="locations-illustration" src="/wp-content/uploads/2017/09/sps-dgtl-cities.svg">
                <figcaption id="locations-titles">
                    <div id="location-los-angeles" class="location"><?php the_field('location_los_angeles'); ?></div>
                    <div id="location-san-antonio" class="location"><?php the_field('location_san_antonio'); ?></div>
                    <div id="location-tampa" class="location"><?php the_field('location_tampa'); ?></div>
                </figcaption>
            </figure>
            <h2><?php the_field('locations_info'); ?></h2>
        </article>
        <article id="our-story">
            <div class="container">
                <h2><?php the_field('our_story_title'); ?></h2>
                <?php the_field('our_story_content'); ?>
                <p>
                    <a href="/story" class="cta outline-light arrow"><?php the_field('our_story_cta'); ?></a>
                </p>
            </div>
        </article>
        <article id="connect">
            <h2><?php the_field('connect_title'); ?></h2>
            <?php the_field('connect_content'); ?>
			<div class="cards">
                <a href="/creative-services">Creative Services<img src="/wp-content/uploads/2017/11/easel.png"></a>
                <a href="/creative-content-distribution">Creative Content Distribution<img src="/wp-content/uploads/2017/11/typewriter.png"></a>
                <a href="/social-advertising">Social Advertising<img src="/wp-content/uploads/2017/11/billboard.png"></a>
            </div>
        </article>
        <article id="engage">
            <h2><?php the_field('engage_title'); ?></h2>
            <?php the_field('engage_content'); ?>
			<div class="cards">
                <a href="/social-engagement-customer-care">Social Engagement &amp; Customer Care<img src="/wp-content/uploads/2017/11/phone.png"></a>
            </div>
        </article>
        <article id="grow">
            <h2><?php the_field('grow_title'); ?></h2>
            <?php the_field('grow_content'); ?>
        </article>
        <article id="case-study">
            <h3 class="work-title">
                <span id="case-study-tag">Case Study</span>
                <b>Major Multinational Retailer</b>
                <i>Natural Disaster Response</i>
            </h3>
            <div class="work-sections">
                <section class="approach">
                    <div>
                        <p>We provided 24/7 real-time engagement on Facebook, Twitter, and Instagram.</p>
                        <p>We completed 100,000 engagements of incoming social media in 30 days. </p>
                        <p>We informed customers of up-to-date store closings and emergency resources. </p>
                    </div>
                    <p><b>In less than two weeks</b>, one client with a strong presence in the areas impacted by a natural disaster saw their total audience grow <b>5 percent</b> to a total of over <b>1.7 million.</b></p>
                </section>
                <section class="results">
                    <div class="chart-labels">
                        <h4 class="chart-label total">
                            <span class="text">Total Audience Growth</span>
                            <span class="value">84,128</span>
                            <span class="percent">4.9%</span>
                        </h4>
                        <h4 class="chart-label growth">
                            <span class="text">Audience Growth by Social Channel</span>
                            <section class="networks">
                                <section class="facebook">
                                    <span class="value">77,889</span>
                                    <span class="percent">5.7%</span>
                                    <span class="text">Facebook</span>
                                </section>
                                <section class="instagram">
                                    <span class="value">1,461</span>
                                    <span class="percent">2.2%</span>
                                    <span class="text">Instagram</span>
                                </section>
                                <section class="twitter">
                                    <span class="value">4,668</span>
                                    <span class="percent">2.2%</span>
                                    <span class="text">Twitter</span>
                                </section>
                            </section>
                        </h4>
                        <h4 class="chart-label over-time">Audience Growth Over Time</h4>
                    </div>
                    <canvas id="audience-growth-chart" class="chart"></canvas>
                </section>
                <button class="work-sections-toggle cta alt">
                    <span>The Results</span>
                    <span>The Approach</span>
                </button>
            </div>
        </article>
		<?php get_sidebar( 'newsletter' ); ?>
        <article id="blog">
            <h2><?php _e('Blog', 'spsdgtl'); ?></h2>
            <section id="blog-posts"><?php
                $tri_query = new WP_Query( array( 'posts_per_page' => 3 ) );
	            if ( $tri_query->have_posts() ) :
		            while ( $tri_query->have_posts() ) : $tri_query->the_post(); // The Loop ?>
                        <a href="<?php the_permalink(); ?>" class="blog-post" rel="bookmark"><?php
                        if ( get_the_ID() === 426 ): ?>
                            <img src="/wp-content/uploads/2017/09/sps-dgtl-logo-a-transparent.svg" style="background: linear-gradient(to bottom, var(--branding-blue) 0%, var(--branding-yellow) 100%);"><?php
                        else: ?>
                            <img src="<?php the_post_thumbnail_url( 'large' ); ?>"><?php
                        endif; ?>
				            <?php the_title( '<h4 class="title">', '</h4>'); ?>
                        </a>
			            <?php
		            endwhile;
	            else: ?>
                    <a class="blog-post" href="#!">
                        <img src="">
                        <h4 class="title">Blog post 3</h4>
                    </a>
                    <a class="blog-post" href="#!">
                        <img src="">
                        <h4 class="title">Blog post 2</h4>
                    </a>
                    <a class="blog-post" href="#!">
                        <img src="">
                        <h4 class="title">Blog post 1</h4>
                    </a><?php
	            endif; ?>
            </section>
            <a href="/blog" class="cta arrow alt">View More</a>
        </article>
<?php
$script = <<<'SCRIPT'
        ctx = document.getElementById("audience-growth-chart").getContext("2d");

        Chart.defaults.global.defaultFontFamily = 'filson-pro, sans-serif';
        Chart.defaults.global.defaultFontSize = 16;
        Chart.defaults.global.defaultFontColor = 'white';

        new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [
                    {
                        label: 'Total Audience (Followers, Fans, Friends)',
                        data: [1649606, 1654314, 1656993, 1662182, 1678936, 1695530, 1708365, 1711768, 1712673, 1713611, 1714223, 1724586, 1732506, 1733162],
                        yAxisID: 'total',
                        type: 'line',
                        backgroundColor: 'rgba(255, 255, 255, 0.2)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    },
                    {
                        label: 'Facebook',
                        data: [485, 5021, 2458, 4546, 16205, 15737, 13290, 2248, 662, 556, 591, 10767, 7933, 614],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(145, 168, 210, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Instagram',
                        data: [21, 13, 101, 125, 147, 140, 134, 244, 49, 274, 163, 11, 93, 114],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(175, 191, 222, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }, {
                        label: 'Twitter',
                        data: [73, 359, 454, 168, 852, 452, 334, 106, 51, 183, 197, 114, 135, 222],
                        yAxisID: 'growth',
                        backgroundColor: 'rgba(202, 219, 255, 1.0)',
                        borderColor: 'rgba(255, 255, 255, 1.0)'
                    }
                ],
                labels: ['Aug 24', 'Aug 25', 'Aug 26', 'Aug 27', 'Aug 28', 'Aug 29', 'Aug 30', 'Aug 31', 'Sept 1', 'Sept 2', 'Sept 3', 'Sept 4', 'Sept 5', 'Sept 6']
            },
            options: {
                legend: {
                    display: true,
                    position: 'bottom'
                },
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        id: 'growth',
                        type: 'linear',
                        position: 'right',
                        stacked: true,
                        ticks: {
                            stepSize: 5000,
                            max: 25000
                        }
                    }, {
                        id: 'total',
                        type: 'linear',
                        position: 'left',
                        stacked: true,
                        ticks: {
                            stepSize: 25000,
                            min: 1625000,
                            max: 1750000
                        }
                    }
                    ]
                }
            }
        });

        var sections = document.querySelector('.work-sections');
        document.querySelector('.work-sections-toggle').addEventListener('click', function () {
            sections.classList.toggle('second-section');
        });
SCRIPT;

wp_enqueue_script( 'spsdgtl-chartjs', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js', array(), '2.7.1', true );
wp_add_inline_script('spsdgtl-chartjs', $script);

get_footer();