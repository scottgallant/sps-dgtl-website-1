<?php
/**
 * SPS DGTL functions and definitions
 */

if ( ! function_exists( 'spsdgtl_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function spsdgtl_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 */
		load_theme_textdomain( 'spsdgtl', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'spsdgtl' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'spsdgtl_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function spsdgtl_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'spsdgtl_content_width', 1400 );
}
add_action( 'after_setup_theme', 'spsdgtl_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function spsdgtl_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Newsletter', 'spsdgtl' ),
		'id'            => 'newsletter',
		'description'   => esc_html__( 'Add widgets here.', 'spsdgtl' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'spsdgtl_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function spsdgtl_scripts() {
	wp_enqueue_style( 'spsdgtl-style', get_stylesheet_uri() );

	wp_enqueue_style( 'spsdgtl-fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css');

	wp_add_inline_script( 'jquery-migrate', 'var $ = jQuery;', 'after' );

	wp_enqueue_script( 'spsdgtl-waypoints', get_stylesheet_directory_uri() . '/js/waypoints.js', null, null, true);

	wp_enqueue_script( 'spsdgtl-main', get_stylesheet_directory_uri() . '/main.js', array( 'jquery', 'spsdgtl-waypoints' ), null, true);

	wp_enqueue_script( 'spsdgtl-hubspot', '//js.hs-scripts.com/4063536.js', null, null, true);

	wp_enqueue_script( 'spsdgtl-facebook-pixel', 'https://connect.facebook.net/en_US/fbevents.js', null, null, true);

	wp_enqueue_script( 'spsdgtl-typekit', 'https://use.typekit.net/nrr2csk.js', null, '1.0' );
	wp_add_inline_script( 'spsdgtl-typekit', 'try{Typekit.load({ async: true });}catch(e){}' );
}
add_action( 'wp_enqueue_scripts', 'spsdgtl_scripts' );

function spsdgtl_add_to_script( $tag, $handle, $src ) {
	if ( $handle === 'spsdgtl-hubspot' ) {
		$tag = '<script type="text/javascript" id="hs-script-loader" async defer src="' . esc_url( $src ) . '"></script>' . "\n";
	}
	elseif ( $handle === 'spsdgtl-facebook-pixel' ) {
		$tag = <<<'FBPIXELSCRIPT'
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s) {
      if(f.fbq)return;n=f.fbq=function(){
          n.callMethod?
  	 	  n.callMethod.apply(n,arguments):n.queue.push(arguments)
      };
  	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  	  n.queue=[];t=b.createElement(e);t.async=!0;
  	  t.src=v;s=b.getElementsByTagName(e)[0];
  	  s.parentNode.insertBefore(t,s);
  }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
  
  fbq('init', '123699011680310');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=123699011680310&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->

FBPIXELSCRIPT;
	}

	return $tag;
}
add_filter( 'script_loader_tag', 'spsdgtl_add_to_script', 10, 3);