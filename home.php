<?php
/*
 * Blog Home Template
 */

get_header();
?>
	<article>
		<h2 id="blog-title"><?php _e( 'Blog', 'spsdgtl' ); ?></h2>
		<section id="blog-posts"><?php
	if ( have_posts() ) :
		while ( have_posts() ) : the_post(); // The Loop ?>
			<a href="<?php the_permalink(); ?>" class="blog-post" rel="bookmark"><?php
				if ( get_the_ID() === 426 ): ?>
                    <img src="/wp-content/uploads/2017/09/sps-dgtl-logo-a-transparent.svg" style="background: linear-gradient(to bottom, var(--branding-blue) 0%, var(--branding-yellow) 100%);"><?php
				else: ?>
                    <img src="<?php the_post_thumbnail_url(  get_the_ID(), 'medium_large' ); ?>"><?php
				endif; ?>
				<?php the_title( '<h4 class="title">', '</h4>'); ?>
			</a>
			<?php
		endwhile;
	else: ?>
			<a class="blog-post" href="#!">
				<img src="">
				<h4 class="title">Blog post 5</h4>
			</a>
			<a class="blog-post" href="#!">
				<img src="">
				<h4 class="title">Blog post 4</h4>
			</a>
			<a class="blog-post" href="#!">
				<img src="">
				<h4 class="title">Blog post 3</h4>
			</a>
			<a class="blog-post" href="#!">
				<img src="">
				<h4 class="title">Blog post 2</h4>
			</a>
			<a class="blog-post" href="#!">
				<img src="">
				<h4 class="title">Blog post 1</h4>
			</a><?php
	endif; ?>
		</section>
	</article>
	<div id="newsletter-signup">
		<h2>Receive our newsletter!</h2>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <div id="newsletter-signup-inputs">
            <script>
                hbspt.forms.create({
                    portalId: '4063536',
                    formId: 'c36f0ae8-8732-460c-a1c1-54815e29d7ed',
                    css: ''
                });
            </script>
        </div>
	</div>
<?php
get_footer();