<?php
/**
 * Theme header.
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<title><?php
        if ( is_front_page() ) :
            ?>SPS DGTL - Connect. Engage. Grow.<?php
        elseif ( is_home() ):
	        ?>Blog - SPS DGTL<?php
        else:
	        the_title(); ?> - SPS DGTL<?php
        endif;
    ?></title>
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
	<link href="/wp-content/uploads/2017/09/sps-dgtl-logo-a-minimal.svg" rel="icon">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if ( !is_page_template('page_bio.php') ) : ?>
<nav id="site-navigation">
	<div id="menu-container">
		<a href="https://spsdgtl.com/#main" id="home-link"><img src="/wp-content/uploads/2017/09/sps-dgtl-logo-a-transparent.svg" alt="SPS DGTL"></a>
		<div id="site-menu">
            <a href="/creative-services">Services</a>
			<a href="/about">About</a>
			<a href="/learn">Learn</a>
			<a href="/contact">Contact</a>
		</div>
	</div>
</nav>
<button id="menu-toggle" class="fa fa-bars"></button>
<?php endif; ?>
<?php if ( is_front_page() ) : ?><div id="site-container"><?php endif; ?>
<header id="site-header">
<?php   if ( is_page_template('page_bio.php') ) : ?>
    <a href="https://spsdgtl.com/#main" id="home-link-top"><img src="/wp-content/uploads/2017/09/sps-dgtl-logo-a-transparent.svg"></a>
<?php   elseif ( is_front_page() ) : ?>
    <object type="image/svg+xml" data="/wp-content/uploads/2017/09/sps-dgtl-logo-transparent-animated.svg">SPS DGTL</object>
<?php   else : ?>
    <a href="https://spsdgtl.com/#main" id="home-link-top"><img src="/wp-content/uploads/2017/11/sps-dgtl-logo-b-minimal.svg"></a>
    <nav id="site-menu-top">
        <div>
            Services
            <ul>
                <li><a href="/creative-services">Creative Services</a></li>
                <li><a href="/creative-content-distribution">Creative Content Distribution</a></li>
                <li><a href="/social-engagement-customer-care">Social Engagement &amp; Customer Care</a></li>
                <li><a href="/social-advertising">Social Advertising</a></li>
            </ul>
        </div>
        <a href="/about">About</a>
        <a href="/learn">Learn</a>
        <a href="/contact">Contact</a>
    </nav>
<?php   endif; ?>
</header>
<main id="main">